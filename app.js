var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var dbPath = "mongodb://localhost/configuration_management_db";
var db = mongoose.connect(dbPath);   // connect with database

// check db connection is opened
mongoose.connection.once('open', function(){
    console.log('database open connection succeeded');  
});

// middlewares for body parsing
app.use(bodyParser.json({limit:'10mb', extended:true}));
app.use(bodyParser.urlencoded({limit:'10mb', extended:true}));

// allow cors
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

var application = require('./applicationModel');

// GET : all applications
app.get("/applications", function(req, res){
    application.find(function (err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.send(result);
        }
    });

});

// GET : get a particular application details
app.get("/application/:id", function(req, res) {
    application.findOne({'_id' : req.params.id}, function (err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.send(result);
        }
    });

});

// POST : create new application with configuration
app.post("/application/create", function(req, res){
    console.log(req.body.name);
    var newApplication = new application({
        namespace: req.body.namespace,
        name: req.body.name,
        configuration: req.body.configuration
    });

    newApplication.save(function (err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.send(result);
        }
    });
});

// PUT : update application details
app.put("/application/:id/update", function(req, res) {
    var updateApplication = {
        namespace: req.body.namespace,
        name: req.body.name,
        configuration: req.body.configuration
    };
    application.findOneAndUpdate({'_id': req.params.id}, updateApplication, {upsert:true}, function (err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.send(result);
        }
    });
});

// DELETE : remove application from database
app.delete("/application/:id/delete", function(req, res) {
    application.remove({'_id': req.params.id}, function (err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.send(result);
        }
    });
});

//generic Error Handling middleware
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
  res.status(404).send('Hey...I have not created this page')
  next(err);
});

// listening on a port
app.listen(8000, function () {
    console.log("app listening on port 8000");
});
