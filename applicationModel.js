var mongoose = require('mongoose');
var applicationSchema = new mongoose.Schema({
    namespace: {type:String, default: '', required:true},
    name: {type:String, default: '', required:true},
    configuration: {type:JSON, default: ''}
});

var application = mongoose.model("applications", applicationSchema);

module.exports = application;
